Source: mapclassify
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               jdupes,
               libjs-mathjax,
               libjs-requirejs,
               pybuild-plugin-pyproject,
               python3-all,
               python3-doc <!nodoc>,
               python3-geopandas,
               python3-libpysal,
               python-libpysal-doc <!nodoc>,
               python3-nbsphinx <!nodoc>,
               python3-networkx <!nocheck>,
               python3-numpy <!nocheck>,
               python-numpy-doc <!nodoc>,
               python3-numpydoc <!nodoc>,
               python3-palettable <!nocheck>,
               python3-pandas <!nocheck>,
               python-pandas-doc <!nodoc>,
               python3-pyogrio,
               python3-pytest,
               python3-pytest-xdist <!nocheck>,
               python3-scipy <!nocheck>,
               python-scipy-doc <!nodoc>,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sklearn <!nocheck> <!nodoc>,
               dh-sequence-sphinxdoc <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-gallery <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-sphinxcontrib.bibtex <!nodoc>
Standards-Version: 4.7.0
Homepage: https://github.com/pysal/mapclassify
Vcs-Browser: https://salsa.debian.org/science-team/mapclassify
Vcs-Git: https://salsa.debian.org/science-team/mapclassify.git
Testsuite: autopkgtest-pkg-pybuild

Package: python3-mapclassify
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-mapclassify-doc
Description: Classification Schemes for Choropleth Maps
 Library that provides tools for classifying geographic data on maps.
 It allows you to divide a spatial dataset into different classes or
 ranges, making it easier to visualize and analyze this data on maps.
 .
 This library is especially useful for grouping and categorizing geographic
 information, helping to identify patterns and trends in data.
 .
 It supports multiple classification methods such as Jenks Natural Breaks,
 Equal Interval and Quantile, among others, allowing users to choose the
 best approach for their specific analysis. Additionally, the library is
 integrated with other popular geographic data analysis tools, making it
 easy to incorporate their functionality into existing workflows.
 .
 This package provides the Python 3.x module

Package: python-mapclassify-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-mathjax,
         libjs-requirejs,
         ${misc:Depends},
         ${sphinxdoc:Depends}
Suggests: python-libpysal-doc,
          python-numpy-doc,
          python-pandas-doc,
          python-scipy-doc,
          python3-doc
Description: Classification Schemes for Choropleth Maps (common documentation)
 Library that provides tools for classifying geographic data on maps.
 It allows you to divide a spatial dataset into different classes or
 ranges, making it easier to visualize and analyze this data on maps.
 .
 This library is especially useful for grouping and categorizing geographic
 information, helping to identify patterns and trends in data.
 .
 It supports multiple classification methods such as Jenks Natural Breaks,
 Equal Interval and Quantile, among others, allowing users to choose the
 best approach for their specific analysis. Additionally, the library is
 integrated with other popular geographic data analysis tools, making it
 easy to incorporate their functionality into existing workflows.
 .
 This package contains HTML documentation, incorporates instructions on how
 to install and configure and use this module with mapclassify.
