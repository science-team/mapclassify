Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/pysal/mapclassify
Upstream-Name: mapclassify
Upstream-Contact: PySAL-mapclassify Developers <pysal-dev@googlegroups.com>

Files: *
Copyright: 2018 PySAL-mapclassify Developers <pysal-dev@googlegroups.com>
License: BSD-3-clause

Files: mapclassify/greedy.py
Copyright: 2017 Nyall Dawson <nyall.dawson@gmail.com>
           2019 Martin Fleischmann <martin@martinfleischmann.net>
License: BSD-3-clause

Files: mapclassify/classifiers.py
Copyright: Sergio J. Rey
License: BSD-3-clause

Files: mapclassify/_classify_API.py
Copyright: Stefanie Lumnitz <stefanie.lumitz@gmail.com>
License: BSD-3-Clause

Files:  debian/*
Copyright: 2023 Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>
           2023 Nilesh Patra <nilesh@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
