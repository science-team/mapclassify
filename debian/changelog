mapclassify (2.8.1-3) unstable; urgency=medium

  * debian/control: Removed <!nocheck> to fix s390x architecture tests

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 19 Oct 2024 19:27:07 -0300

mapclassify (2.8.1-2) unstable; urgency=medium

  * debian/control: Added Build-depends python3-pyogrio

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Thu, 17 Oct 2024 20:42:24 -0300

mapclassify (2.8.1-1) unstable; urgency=medium

  * New upstream version 2.8.1
  * debian/python3-mapclassify.lintian-overrides: Removed unnecessary file

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Thu, 17 Oct 2024 20:40:45 -0300

mapclassify (2.8.0-1) unstable; urgency=medium

  * New upstream version 2.8.0

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 06 Aug 2024 22:02:00 -0300

mapclassify (2.7.0-1) unstable; urgency=medium

  * New upstream version 2.7.0
  * debian/control:
    - Added Build-Depends jdupes
    - Bumped Standards-Version to 4.7.0
  * debian/patches/fixed-path-file-tests.patch:
    - Fixed file path for tests
  * debian/rules:
    - Fixed duplicate files
    - Fixed file path to enable testing
    - Ignored because it tries to connect externally (Closes: #1067014)

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 06 Jul 2024 12:13:54 -0300

mapclassify (2.6.1-5) unstable; urgency=medium

  * Team upload.
  * d/control: mark docs- or test-only dependencies. Closes: #1057383
  * Added intersphinx patch to connect to related documentation
    packages.

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 22 May 2024 13:07:05 +0200

mapclassify (2.6.1-4) UNRELEASED; urgency=medium

  * Team upload.
  * d/control: build-dep on setuptools-scm to fix wrong "0.0.0" version

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 25 Apr 2024 21:41:38 +0300

mapclassify (2.6.1-3) unstable; urgency=medium

  * debian/control: added Testsuite: autopkgtest-pkg-pybuild
  * debian/tests/control: replaced by autopkgtest from debian/control

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sun, 31 Dec 2023 07:27:40 -0300

mapclassify (2.6.1-2) unstable; urgency=medium

  * debian/rules: removed build failed tests
  * debian/tests: Build error fixed

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 18 Oct 2023 07:13:10 -0300

mapclassify (2.6.1-1) unstable; urgency=medium

  * New upstream version 2.6.1
  * debian/copyright: File 'versioneer.py' has been removed in the new version
  * debian/rules: Bug fixed in testing

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 14 Oct 2023 21:57:14 -0300

mapclassify (2.6.0-2) unstable; urgency=medium

  * Team Upload.
  * Point VCS URLs to science team

 -- Nilesh Patra <nilesh@debian.org>  Wed, 30 Aug 2023 19:34:49 +0530

mapclassify (2.6.0-1) unstable; urgency=medium

  [ Josenilson Ferreira da Silva, Nilesh Patra ]
  * Initial release. (Closes: #1043540)

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Mon, 21 Aug 2023 19:36:11 +0530
